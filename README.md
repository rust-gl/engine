# RustGL
Rustgl is a rust wrapper around webgl.  
The purpose of this api is to bring some of rusts safety features to webgl.  
This includes :  
1. Program creation.
2. Program binding.
3. Buffer binding.
4. Uniform and attribute binding.

