#![deny(warnings)]
#![allow(incomplete_features)]
#![feature(
    never_type,
    vec_spare_capacity,
    maybe_uninit_uninit_array,
    maybe_uninit_array_assume_init
)]

pub mod attribute;
pub mod binder;
pub mod context;
pub mod math;
pub mod obj;
pub mod object;
pub mod program;
pub mod uniform;
pub mod using;
pub mod derive {
    pub use rust_gl_proc::*;
}

pub type GL = web_sys::WebGl2RenderingContext;
