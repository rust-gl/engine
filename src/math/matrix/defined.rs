use super::Matrix;
use std::mem::swap;

pub type Matrix2x2<T> = Matrix<T, 2, 2>;
pub type Matrix2x3<T> = Matrix<T, 2, 3>;
pub type Matrix2x4<T> = Matrix<T, 2, 4>;

pub type Matrix3x2<T> = Matrix<T, 3, 2>;
pub type Matrix3x3<T> = Matrix<T, 3, 3>;
pub type Matrix3x4<T> = Matrix<T, 3, 4>;

pub type Matrix4x2<T> = Matrix<T, 4, 2>;
pub type Matrix4x3<T> = Matrix<T, 4, 3>;
pub type Matrix4x4<T> = Matrix<T, 4, 4>;

pub type MatrixSq<T, const N: usize> = Matrix<T, N, N>;
pub type Matrix2<T> = MatrixSq<T, 2>;
pub type Matrix3<T> = MatrixSq<T, 3>;
pub type Matrix4<T> = MatrixSq<T, 4>;

impl<T, const N: usize> Matrix<T, N, N>
where
    T: From<u8>,
{
    ///Construct a translation matrix.
    pub fn translate(mut values: [T; N]) -> Self {
        let mut output = Self::identity();
        for (index, value) in values.iter_mut().enumerate() {
            unsafe {
                swap(
                    output.get_unchecked_mut(index).get_unchecked_mut(N - 1),
                    value,
                );
            }
        }
        output
    }
    ///Construct a scalation matrix.
    pub fn scale(mut values: [T; N]) -> Self {
        let mut output = Self::identity();
        for (index, value) in values.iter_mut().enumerate() {
            unsafe {
                swap(
                    output.get_unchecked_mut(index).get_unchecked_mut(index),
                    value,
                );
            }
        }
        output
    }
}

use ::core::ops::*;
impl Matrix4<f64>
where
    f64: Mul<Output = f64>,
{
    ///Construct a 4 by 4 rotation matrix around the x-axis.
    pub fn rotate_x(x: f64, w: f64) -> Self {
        matrix! {
            1., 0., 0., 0.;
            0., x.cos(), -x.sin(), 0.;
            0., x.sin(), x.cos(), 0.;
            0., 0., 0., w
        }
    }
    ///Construct a 4 by 4 rotation matrix around the y-axis.
    pub fn rotate_y(y: f64, w: f64) -> Self {
        matrix! {
            y.cos(), 0., y.sin(), 0.;
            0., 1., 0., 0.;
            -y.sin(), 0.0, y.cos(), 0.0;
            0.0, 0.0, 0.0, w
        }
    }
    ///Construct a 4 by 4 rotation matrix around the y-axis.
    pub fn rotate_z(z: f64, w: f64) -> Self {
        matrix! {
            z.cos(), -z.sin(), 0., 0.;
            z.sin(), z.cos(), 0., 0.;
            0., 0., 1., 0.;
            0., 0., 0., w
        }
    }

    ///Construct a 4 by 4 rotation matrix.
    pub fn rotate(x: f64, y: f64, z: f64, w: f64) -> Self {
        let mut output: Self =
            Self::rotate_x(x, 1.) * Self::rotate_y(y, 1.) * Self::rotate_z(z, 1.);
        unsafe {
            *output.get_unchecked_mut(3).get_unchecked_mut(3) = w;
        }
        output
    }
}

impl Matrix4<f64> {
    ///Constrct a perspective matrix.
    pub fn perspective(fov: f64, aspect: f64, near: f64, far: f64) -> Matrix4<f64> {
        let y_scale = 1.0 / (fov / 2.).tan();
        let x_scale = y_scale / aspect;
        let dist = near - far;
        matrix! {
            x_scale, 0., 0., 0.;
            0., y_scale, 0., 0.;
            0., 0., (far + near) / dist, -1.;
            0., 0., 2.*far*near / dist, 0.
        }
    }
}
