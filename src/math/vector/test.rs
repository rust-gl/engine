#[allow(unused_imports)]
use crate::math::Vector;

#[test]
fn macro_test() {
    let control = Vector::from([0, 1, 2, 3]);
    let test = vector!(0, 1, 2, 3);
    assert_eq!(control, test)
}
