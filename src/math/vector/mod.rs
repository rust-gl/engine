#[macro_export]
macro_rules! vector {
    ($($e:expr),*) => {
        {
            use $crate::math::vector::Vector;
            Vector::from([$($e),*])
        }
    }
}
pub mod defined;
mod test;
pub use defined::*;

use derive_more::*;

#[derive(Clone, Debug, Eq, PartialEq, From, Deref, DerefMut)]
pub struct Vector<T, const N: usize>([T; N]);

use std::fmt;
impl<T, const N: usize> fmt::Display for Vector<T, N>
where
    T: fmt::Display,
{
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        let mut iter = self.as_slice().iter();
        let _ = write! {fmt, "["}?;

        if let Some(x) = iter.next() {
            let _ = write! {fmt, "{}", x}?;
        }
        for x in iter {
            let _ = write! {fmt, ", {}", x}?;
        }
        write! {fmt, "]"}
    }
}

use std::mem::{swap, MaybeUninit};
impl<T, const N: usize> Vector<T, N> {
    ///Map every element of the vector with the specified function.
    pub fn map<F, R>(self, func: F) -> Vector<R, N>
    where
        F: Fn(T) -> R,
    {
        self.0.map(func).into()
    }
    ///Combine 2 vectors into 1 with a specified function.
    pub fn merge<F, T2, R>(self, rhs: Vector<T2, N>, func: F) -> Vector<R, N>
    where
        F: Fn(T, T2) -> R,
    {
        let mut output: Vector<R, N> = unsafe { MaybeUninit::uninit().assume_init() };
        for (i, (x, y)) in self.into_iter().zip(rhs.into_iter()).enumerate() {
            output[i] = func(x, y);
        }
        output
    }
    ///Return a slice containing all the elements of the vector.
    pub fn as_slice(&self) -> &[T] {
        &self.0
    }
}

impl<T> Vector<T, 1> {
    ///Cast the vector of size 1, to a scalar.
    pub fn to_single(mut self) -> T {
        let mut output = unsafe { MaybeUninit::uninit().assume_init() };
        unsafe { swap(&mut output, self.0.get_unchecked_mut(0)) };
        output
    }
}

impl<const N: usize> Vector<f64, N> {
    ///Calculate the euclidian distance of the vector.
    pub fn euclid_dist(self) -> Option<f64> {
        let mut iter = self.into_iter().map(|x| x.powi(2));

        let first = iter.next()?;

        iter.fold(first, Add::add).sqrt().into()
    }
    ///Normalize the vector, equivalent to self * 1/self.euclid_dist()
    pub fn norm(self) -> Option<Self>
    where
        Self: Clone,
    {
        let l = self.clone().euclid_dist()?;
        self.map(|x| x / l).into()
    }
}

impl<T, const N: usize> IntoIterator for Vector<T, N> {
    type Item = T;
    type IntoIter = core::array::IntoIter<T, N>;

    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter::new(self.0)
    }
}

use core::ops::*;

///Calculates the inner product of 2 vectors, will only return None if N is 0.
impl<T, T2, const N: usize> Mul for Vector<T, N>
where
    T: Mul<Output = T2>,
    T2: Add<Output = T2>,
{
    type Output = Option<T2>;

    fn mul(self, rhs: Self) -> Self::Output {
        let mut iter = self.into_iter().zip(rhs.into_iter()).map(|(x, y)| x * y);
        let first = iter.next()?;
        iter.fold(first, Add::add).into()
    }
}

impl<T, T2, R, const N: usize> Add<Vector<T2, N>> for Vector<T, N>
where
    T: Add<T2, Output = R>,
{
    type Output = Vector<R, N>;

    fn add(self, rhs: Vector<T2, N>) -> Self::Output {
        self.merge(rhs, Add::add)
    }
}

impl<T, T2, R, const N: usize> Sub<Vector<T2, N>> for Vector<T, N>
where
    T: Sub<T2, Output = R>,
{
    type Output = Vector<R, N>;

    fn sub(self, rhs: Vector<T2, N>) -> Self::Output {
        self.merge(rhs, Sub::sub)
    }
}
