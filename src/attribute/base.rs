use super::*;
use std::rc::Rc;
use web_sys::WebGlProgram;
extern crate derive_more;
use derive_more::*;

#[derive(Debug, Deref)]
pub struct Base<T> {
    index: Index,
    #[allow(unused)]
    name: String,
    gl: Rc<GL>,
    #[deref]
    constant: T,
}

impl<T> Attribute<T> for Base<T>
where
    T: GLType,
{
    fn index(&self) -> &Index {
        &self.index
    }
    fn swap(&mut self, mut constant: T) -> T {
        use core::mem::swap;
        swap(&mut self.constant, &mut constant);
        self.constant.constant(&*self.gl, self.index);
        constant
    }
}

#[derive(Debug, Display)]
pub enum Error {
    ///If the given attribute does not exist.
    #[display(fmt = "Attribute '{}' does not exists", name)]
    NonExistent {
        name: String,
        index: i32,
    },
    Attribute(gltype::Error),
    ///If opengl cannot retrive the attribute information.
    #[display(fmt = "Attribute '{}' at '{}' is not active", name, index)]
    ActiveAttrib {
        name: String,
        index: u32,
    },
}

impl std::error::Error for Error {}

use std::collections::HashMap;

impl<T> ToAttribute for T
where
    T: GLType,
    Base<T>: Attribute<T>,
{
    type Output = Base<T>;
    type Error = Error;
    fn to_attribute(
        self,
        gl: Rc<GL>,
        program: &WebGlProgram,
        name: &str,
        map: &mut HashMap<String, WebGlActiveInfo>,
    ) -> Result<Self::Output, Self::Error> {
        let name = String::from(name);
        //Get the index of the attrrbute from the gl context.
        let index = gl.get_attrib_location(&program, &name);

        //If number is negative then the attribute does not exists.
        let index = if 0 <= index {
            Ok(index as u32)
        } else {
            Err(Error::NonExistent {
                name: name.clone(),
                index,
            })
        }?;

        let attribute = map.remove(&name).ok_or(Error::ActiveAttrib {
            name: name.clone(),
            index,
        })?;

        //Compare if the type is compatible with the attribute.
        T::compatible(&attribute).map_err(Error::Attribute)?;

        use core::mem::MaybeUninit;
        let mut base = Base::<T> {
            index,
            name,
            gl,
            constant: unsafe { MaybeUninit::uninit().assume_init() },
        };

        base.swap(self);

        Ok(base)
    }
}
