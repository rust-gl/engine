use derive_more::*;
///Opengl pointer specifying the location of the data.
#[derive(Default, Debug, Clone)]
pub struct Pointer {
    ///If the attribute is to recive normalized values.
    pub normalized: bool,
    ///The distance in bytes between 2 elements.
    pub stride: i32,
    ///Where the data for the attribute in the given buffer starts.
    pub offset: i32,
}

use super::Index;
use core::marker::PhantomData;
#[derive(Debug, Deref)]
pub struct AttributePointer<'attribute, T> {
    ///The the location of the attribute.
    pub index: &'attribute Index,
    ///Data Pointer.
    #[deref]
    pub pointer: Pointer,
    //The type of the attribute pointer
    phantom: PhantomData<(T,)>,
}

impl<'attribute, T> AttributePointer<'attribute, T> {
    pub fn new(index: &'attribute Index, pointer: Pointer) -> Self {
        Self {
            index,
            pointer,
            phantom: PhantomData,
        }
    }
}
