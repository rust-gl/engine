pub mod gltype;
pub use gltype::*;

pub mod pointer;
pub use pointer::*;

pub mod base;
pub use base::*;

use std::collections::HashMap;
use web_sys::{WebGlActiveInfo, WebGlProgram};

use super::GL;
use std::rc::Rc;

pub type Index = u32;
use std::error::Error;

/// Constructs opengl attributes.
pub trait ToAttribute {
    ///If the output value is a leaf, it should implement the `Attribute` trait.
    type Output;
    type Error: 'static + Error;
    /// Constructs opengl attribute values.
    ///* `self` The default value of the attribute.
    ///* `name` The name of the attribute.
    ///* `map` A map of attributes, `name` should be removed from this list on creation.
    fn to_attribute(
        self,
        gl: Rc<GL>,
        program: &WebGlProgram,
        name: &str,
        map: &mut HashMap<String, WebGlActiveInfo>,
    ) -> Result<Self::Output, Self::Error>;
}

/// Opengl attribute.
pub trait Attribute<T> {
    ///The index/location of the attribute.
    fn index(&self) -> &Index;
    ///Change the default value of the attibute, returnes the old value.
    fn swap(&mut self, constant: T) -> T;
    ///Construct a attribute pointer.
    fn pointer<'attribute>(
        &'attribute mut self,
        pointer: Pointer,
    ) -> AttributePointer<'attribute, T> {
        AttributePointer::new(self.index(), pointer)
    }
}

impl ToAttribute for () {
    type Output = ();
    type Error = !;
    fn to_attribute(
        self,
        _: Rc<GL>,
        _: &WebGlProgram,
        _: &str,
        _: &mut HashMap<String, WebGlActiveInfo>,
    ) -> Result<Self::Output, Self::Error> {
        Ok(())
    }
}
