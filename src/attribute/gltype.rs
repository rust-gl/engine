use super::Index;
use derive_more::*;
use web_sys::WebGl2RenderingContext;
use web_sys::WebGlActiveInfo;
#[derive(Debug, Error)]
pub enum Error {
    ///If the derived attribute Types does not match.
    Type { expected: u32, actual: u32 },
}

use std::fmt;
impl fmt::Display for Error {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{:?}", self)
    }
}

pub trait GLType {
    const SIZE: i32;
    const TYPE: u32;
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error>;
    fn constant(&self, context: &WebGl2RenderingContext, index: Index);
}

use super::super::math::Vector;

impl GLType for i8 {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1f(index, *self as f32);
    }
}

impl GLType for Vector<i8, 1> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<i8, 2> {
    const SIZE: i32 = 2;
    const TYPE: u32 = { WebGl2RenderingContext::BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT_VEC2 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT_VEC2,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib2fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<i8, 3> {
    const SIZE: i32 = 3;
    const TYPE: u32 = { WebGl2RenderingContext::BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT_VEC3 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT_VEC3,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib3fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<i8, 4> {
    const SIZE: i32 = 4;
    const TYPE: u32 = { WebGl2RenderingContext::BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT_VEC4 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT_VEC4,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib4fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for u8 {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1f(index, *self as f32);
    }
}

impl GLType for Vector<u8, 1> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<u8, 2> {
    const SIZE: i32 = 2;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT_VEC2 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT_VEC2,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib2fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<u8, 3> {
    const SIZE: i32 = 3;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT_VEC3 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT_VEC3,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib3fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<u8, 4> {
    const SIZE: i32 = 4;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_BYTE };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT_VEC4 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT_VEC4,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib4fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for i16 {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1f(index, *self as f32);
    }
}

impl GLType for Vector<i16, 1> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<i16, 2> {
    const SIZE: i32 = 2;
    const TYPE: u32 = { WebGl2RenderingContext::SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT_VEC2 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT_VEC2,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib2fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<i16, 3> {
    const SIZE: i32 = 3;
    const TYPE: u32 = { WebGl2RenderingContext::SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT_VEC3 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT_VEC3,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib3fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<i16, 4> {
    const SIZE: i32 = 4;
    const TYPE: u32 = { WebGl2RenderingContext::SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::INT_VEC4 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::INT_VEC4,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib4fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for u16 {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1f(index, *self as f32);
    }
}

impl GLType for Vector<u16, 1> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<u16, 2> {
    const SIZE: i32 = 2;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT_VEC2 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT_VEC2,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib2fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<u16, 3> {
    const SIZE: i32 = 3;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT_VEC3 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT_VEC3,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib3fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for Vector<u16, 4> {
    const SIZE: i32 = 4;
    const TYPE: u32 = { WebGl2RenderingContext::UNSIGNED_SHORT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::UNSIGNED_INT_VEC4 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::UNSIGNED_INT_VEC4,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib4fv_with_f32_array(index, self.clone().map(|x| x as f32).as_slice())
    }
}

impl GLType for f32 {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::FLOAT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::FLOAT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::FLOAT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1f(index, *self as f32);
    }
}

impl GLType for Vector<f32, 1> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { WebGl2RenderingContext::FLOAT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::FLOAT => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::FLOAT,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib1fv_with_f32_array(index, self.as_slice())
    }
}

impl GLType for Vector<f32, 2> {
    const SIZE: i32 = 2;
    const TYPE: u32 = { WebGl2RenderingContext::FLOAT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::FLOAT_VEC2 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::FLOAT_VEC2,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib2fv_with_f32_array(index, self.as_slice())
    }
}

impl GLType for Vector<f32, 3> {
    const SIZE: i32 = 3;
    const TYPE: u32 = { WebGl2RenderingContext::FLOAT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::FLOAT_VEC3 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::FLOAT_VEC3,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib3fv_with_f32_array(index, self.as_slice())
    }
}

impl GLType for Vector<f32, 4> {
    const SIZE: i32 = 4;
    const TYPE: u32 = { WebGl2RenderingContext::FLOAT };
    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        match info.type_() {
            WebGl2RenderingContext::FLOAT_VEC4 => Ok(()),
            actual => Err(Error::Type {
                expected: WebGl2RenderingContext::FLOAT_VEC4,
                actual,
            }),
        }
    }
    fn constant(&self, context: &WebGl2RenderingContext, index: Index) {
        context.vertex_attrib4fv_with_f32_array(index, self.as_slice())
    }
}
