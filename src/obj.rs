use super::attribute::Pointer;
use super::GL;
use web_sys::WebGlBuffer;

pub struct Ptr {
    pub vertex: Pointer,
    pub normal: Pointer,
}

pub struct Obj {
    pub vertices: WebGlBuffer,
    pub indices: WebGlBuffer,
    pub size: i32,
    pub ptr: Ptr,
}

use super::context::{self, Context};
use derive_more::*;
use std::io::BufRead;

#[derive(Debug, Display, From, Error)]
pub enum Error {
    Obj(obj::ObjError),
    Buffer,
    Context(context::Error),
}

impl Context {
    pub fn load<T>(&mut self, input: T) -> Result<Obj, Error>
    where
        T: BufRead,
    {
        let object: obj::Obj = obj::load_obj(input)?;

        let v = object.vertices;

        let i = object
            .indices
            .into_iter()
            .map(|x| x as u16)
            .collect::<Vec<_>>();

        use crate::binder::TargetBind;
        let mut vertices = self.create_buffer().ok_or(Error::Buffer)?;
        let mut indices = self.create_buffer().ok_or(Error::Buffer)?;

        use crate::binder::allocate::Allocate;
        self.array_buffer
            .bind(&mut vertices)
            .allocate(unsafe { v.align_to().1 }, GL::STATIC_DRAW)?;
        self.element_array_buffer
            .bind(&mut indices)
            .allocate(unsafe { i.align_to().1 }, GL::STATIC_DRAW)?;

        use core::mem::size_of;
        use memoffset::offset_of;
        let vertex = Pointer {
            normalized: false,
            stride: size_of::<obj::Vertex>() as i32,
            offset: 0,
        };

        let normal = Pointer {
            normalized: false,
            stride: size_of::<obj::Vertex>() as i32,
            offset: offset_of!(obj::Vertex, normal) as i32,
        };

        let ptr = Ptr { vertex, normal };

        let obj = Obj {
            vertices,
            indices,
            size: i.len() as i32,
            ptr,
        };

        Ok(obj)
    }
}
