use super::*;

/// Alias for for a binder of the array buffer.
pub type ArrayBinder<'target, 'buffer> =
    Binder<'target, 'buffer, { WebGl2RenderingContext::ARRAY_BUFFER }>;

use super::super::attribute::*;

use std::rc::Rc;

/// Prevents the attribute to be used, during its lifetime.
pub struct BoundPointer<'attribute, 'pointer, T> {
    #[allow(dead_code)]
    pointer: &'attribute mut AttributePointer<'pointer, T>,
    context: Rc<WebGl2RenderingContext>,
}

impl<'attribute, 'pointer, T> Drop for BoundPointer<'attribute, 'pointer, T> {
    fn drop(&mut self) {
        self.context
            .disable_vertex_attrib_array(*self.pointer.index)
    }
}

impl<'target, 'buffer> ArrayBinder<'target, 'buffer> {
    /// Binds the attribute pointer to a given buffer.
    pub fn pointer<'attribute, 'pointer, T>(
        &mut self,
        pointer: &'attribute mut AttributePointer<'pointer, T>,
    ) -> BoundPointer<'attribute, 'pointer, T>
    where
        T: GLType,
    {
        self.target.enable_vertex_attrib_array(*pointer.index);
        self.target.vertex_attrib_pointer_with_i32(
            *pointer.index as u32,
            T::SIZE,
            T::TYPE,
            pointer.normalized,
            pointer.stride,
            pointer.offset,
        );
        BoundPointer {
            pointer,
            context: self.target.0.clone(),
        }
    }
}
