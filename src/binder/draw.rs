use super::super::attribute::*;
use super::io::*;
use super::*;

impl<'binder, 'target, 'buffer>
    Sub<'binder, 'target, 'buffer, { WebGl2RenderingContext::ELEMENT_ARRAY_BUFFER }>
{
    /// Rust like version of the opengl draw_elements function
    pub fn draw<'attribute, 'pointer, T>(&mut self, mode: u32)
    where
        T: GLType,
    {
        use core::mem::size_of;
        let start = self.range.start * size_of::<T>() as i32;
        let span = self.range.end - self.range.start;

        self.binder.target.draw_elements_with_i32(mode, span, T::TYPE, start);
    }
}
