use super::{
    super::context::{Error, GLError},
    Binder,
};

pub trait Allocate<Src> {
    type Error;
    fn allocate(&mut self, src: Src, usage: u32) -> Result<&mut Self, Self::Error>;
}

///Allocate the opengl buffer, and copy the slice to the buffer.
impl<'a, 'target, 'buffer, const TARGET: u32> Allocate<&'a [u8]>
    for Binder<'target, 'buffer, TARGET>
{
    type Error = Error;
    fn allocate(&mut self, src: &'a [u8], usage: u32) -> Result<&mut Self, Error> {
        self.target.buffer_data_with_u8_array(TARGET, src, usage);
        self.target.error()?;
        Ok(self)
    }
}

///Allocate the opengl buffer of the specified size.
impl<'target, 'buffer, const TARGET: u32> Allocate<i32> for Binder<'target, 'buffer, TARGET> {
    type Error = Error;
    fn allocate(&mut self, src: i32, usage: u32) -> Result<&mut Self, Error> {
        self.target.buffer_data_with_i32(TARGET, src, usage);
        self.target.error()?;
        Ok(self)
    }
}

///Allocate of a buffer of the same size as the input buffer and copy the contents of the input buffer to it.
impl<'a, 'target, 'target2, 'buffer, 'buffer2, const TARGET: u32, const TARGET2: u32>
    Allocate<&'a mut Binder<'target2, 'buffer2, TARGET2>> for Binder<'target, 'buffer, TARGET>
{
    type Error = Box<dyn std::error::Error>;
    fn allocate(
        &mut self,
        src: &'a mut Binder<'target2, 'buffer2, TARGET2>,
        usage: u32,
    ) -> Result<&mut Self, Box<dyn std::error::Error>> {
        //Allocate the buffer uninitialized with some data.
        self.allocate(src.len() as i32, usage)?;

        //Copy the contents of the input buffer to the output buffer.
        use super::io::Read;
        let mut dst = self.sub(..);
        src.sub(..).read(&mut dst)?;
        Ok(self)
    }
}
