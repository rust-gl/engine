/// Write trait.
pub trait Write<Src>
where
    Src: ?Sized,
{
    type Error;
    fn write(&mut self, src: &Src) -> Result<(), Self::Error>;
}

/// Read trait.
pub trait Read<Dst>
where
    Dst: ?Sized,
{
    type Error;
    fn read(&mut self, dst: &mut Dst) -> Result<(), Self::Error>;
}

use super::Binder;
use core::ops::Range;
/// Sub io access with the webgl buffer.
pub struct Sub<'binder, 'target, 'buffer, const TARGET: u32> {
    pub binder: &'binder mut Binder<'target, 'buffer, TARGET>,
    pub range: Range<i32>,
}

impl<'binder, 'target, 'buffer, const TARGET: u32> Sub<'binder, 'target, 'buffer, TARGET> {
    /// The length of the sub access in bytes.
    pub fn len(&self) -> usize {
        (self.range.end - self.range.start) as usize
    }
}

use core::ops::RangeBounds;
impl<'target, 'buffer, const TARGET: u32> Binder<'target, 'buffer, TARGET> {
    /// Create a sub access buffer.
    pub fn sub<'binder, T>(&'binder mut self, range: T) -> Sub<'binder, 'target, 'buffer, TARGET>
    where
        T: RangeBounds<i32>,
    {
        use core::ops::Bound::*;
        let range = Range {
            start: match range.start_bound() {
                Included(x) => *x,
                Excluded(x) => x + 1,
                Unbounded => 0,
            },
            end: match range.end_bound() {
                Included(x) => x + 1,
                Excluded(x) => *x,
                Unbounded => self.len() as i32,
            },
        };

        Sub {
            binder: self,
            range,
        }
    }
}

use derive_more::*;
#[derive(Debug, Error)]
pub enum Error {
    UnequalLength { src: usize, dst: usize },
}

use std::fmt;

impl fmt::Display for Error {
    fn fmt(&self, format: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write! {format, "{:?}", self}
    }
}

impl<'binder, 'target, 'buffer, const TARGET: u32> Write<[u8]>
    for Sub<'binder, 'target, 'buffer, TARGET>
{
    type Error = Error;
    fn write(&mut self, src: &[u8]) -> Result<(), Self::Error> {
        if self.len() != src.len() {
            let err = Error::UnequalLength {
                src: src.len(),
                dst: self.len(),
            };
            Err(err)
        } else {
            self.binder
                .target
                .buffer_sub_data_with_i32_and_u8_array(TARGET, self.range.start, src);
            Ok(())
        }
    }
}

impl<'binder, 'target, 'buffer, const TARGET: u32> Read<[u8]>
    for Sub<'binder, 'target, 'buffer, TARGET>
{
    type Error = Error;
    fn read(&mut self, dst: &mut [u8]) -> Result<(), Self::Error> {
        if self.len() != dst.len() {
            let err = Error::UnequalLength {
                src: self.len(),
                dst: dst.len(),
            };
            Err(err)
        } else {
            self.binder
                .target
                .get_buffer_sub_data_with_i32_and_u8_array(TARGET, self.range.start, dst);
            Ok(())
        }
    }
}
use core::mem::MaybeUninit;
impl<'binder, 'target, 'buffer, const TARGET: u32> Read<[MaybeUninit<u8>]>
    for Sub<'binder, 'target, 'buffer, TARGET>
{
    type Error = Error;
    fn read(&mut self, dst: &mut [MaybeUninit<u8>]) -> Result<(), Self::Error> {
        if self.len() != dst.len() {
            let err = Error::UnequalLength {
                src: self.len(),
                dst: dst.len(),
            };
            Err(err)
        } else {
            self.binder
                .target
                .get_buffer_sub_data_with_i32_and_u8_array(TARGET, self.range.start, unsafe {
                    dst.align_to_mut::<u8>().1
                });
            Ok(())
        }
    }
}

impl<'binder, 'target, 'buffer, const TARGET: u32> Read<Vec<u8>>
    for Sub<'binder, 'target, 'buffer, TARGET>
{
    type Error = Error;
    fn read(&mut self, dst: &mut Vec<u8>) -> Result<(), Self::Error> {
        dst.reserve(self.len());
        let len = {
            let slice = &mut dst.spare_capacity_mut()[0..self.len()];
            self.read(slice)?;
            slice.len()
        };
        unsafe {
            dst.set_len(dst.len() + len);
        }
        Ok(())
    }
}

impl<
        'binder,
        'binder2,
        'target,
        'target2,
        'buffer,
        'buffer2,
        const TARGET: u32,
        const TARGET2: u32,
    > Read<Sub<'binder2, 'target2, 'buffer2, TARGET2>> for Sub<'binder, 'target, 'buffer, TARGET>
{
    type Error = Error;
    fn read(
        &mut self,
        dst: &mut Sub<'binder2, 'target2, 'buffer2, TARGET2>,
    ) -> Result<(), Self::Error> {
        if self.len() != dst.len() {
            let err = Error::UnequalLength {
                src: self.len(),
                dst: dst.len(),
            };
            Err(err)
        } else {
            self.binder
                .target
                .copy_buffer_sub_data_with_i32_and_i32_and_i32(
                    TARGET,
                    TARGET2,
                    self.range.start,
                    dst.range.start,
                    dst.len() as i32,
                );
            Ok(())
        }
    }
}
