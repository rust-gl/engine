pub mod allocate;
pub mod array;
pub mod draw;
pub mod io;
use super::context::Target;
use web_sys::{WebGl2RenderingContext, WebGlBuffer};

/// Mutably binds a buffer to a target.
#[allow(dead_code)]
pub struct Binder<'target, 'buffer, const TARGET: u32> {
    ///A mutable reference to the given target, this ensures that as long as this object exists, no other object can use the given target.
    pub target: &'target mut Target<WebGl2RenderingContext, TARGET>,
    ///A mutable reference to the given buffer, this ensures that as long as this object exists, no other object can use the given buffer.
    pub buffer: &'buffer mut WebGlBuffer,
}

impl<'target, 'buffer, const TARGET: u32> Binder<'target, 'buffer, TARGET> {
    ///The length of the buffer in bytes.
    pub fn len(&mut self) -> usize {
        self.target
            .get_buffer_parameter(TARGET, WebGl2RenderingContext::BUFFER_SIZE)
            .as_f64()
            .unwrap() as usize
    }
}

pub trait TargetBind<'target, 'buffer, const TARGET: u32> {
    type Output;
    fn bind(&'target mut self, buffer: &'buffer mut WebGlBuffer) -> Self::Output;
}

impl<'target, 'buffer, const TARGET: u32> TargetBind<'target, 'buffer, TARGET>
    for Target<WebGl2RenderingContext, TARGET>
where
    'buffer: 'target,
{
    type Output = Binder<'target, 'buffer, TARGET>;
    fn bind(&'target mut self, buffer: &'buffer mut WebGlBuffer) -> Self::Output {
        self.bind_buffer(TARGET, Some(buffer));
        Self::Output {
            target: self,
            buffer,
        }
    }
}
