use super::context::Context;

use web_sys::WebGlVertexArrayObject;

pub struct Binder<'context, 'vao> {
    context: &'context mut Context,
    #[allow(dead_code)]
    vao: &'vao mut WebGlVertexArrayObject,
}

impl Context {
    pub fn bind<'context, 'vao>(
        &'context mut self,
        vao: &'vao mut WebGlVertexArrayObject,
    ) -> Binder<'context, 'vao> {
        self.bind_vertex_array(Some(vao));
        Binder { context: self, vao }
    }
}

use core::ops::Deref;
impl<'context, 'vao> Deref for Binder<'context, 'vao> {
    type Target = Context;
    fn deref(&self) -> &Context {
        &self.context
    }
}

use core::ops::DerefMut;
impl<'context, 'vao> DerefMut for Binder<'context, 'vao> {
    fn deref_mut(&mut self) -> &mut Context {
        &mut self.context
    }
}
