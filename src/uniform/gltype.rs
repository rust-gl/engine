use super::*;

use derive_more::*;
#[derive(Debug, Error, PartialEq, Eq, Display)]
pub enum Error {
    ///If the derived GLTypeTypes does not match.
    #[display(fmt = "Expected type '{}', got '{}'", expected, actual)]
    Type { expected: u32, actual: u32 },
    ///If the size of the derived GLTypeTypes does not match.
    #[display(fmt = "Expected size '{}', got '{}'", expected, actual)]
    Size { expected: i32, actual: i32 },
}

use web_sys::WebGlActiveInfo;

///This traits allows for a given GLType Type, to be infered from a type in rust.
pub trait GLType
where
    Self: Sized,
{
    ///If the type is a vector, this will be the number of elements in that vector, otherwise it will just be 1.
    const SIZE: i32;
    ///The gltype that this type converts into.
    const TYPE: u32;

    fn compatible(info: &WebGlActiveInfo) -> Result<(), Error> {
        let expected = Self::TYPE;
        let actual = info.type_();

        //Check if the expected type is equal to the actual type.
        if expected != actual {
            Err(Error::Type { expected, actual })?;
        }

        let expected = Self::SIZE;
        let actual = info.size();

        //Check if the expected size is equal to the actual size.
        if expected != actual {
            Err(Error::Size { expected, actual })?;
        }

        Ok(())
    }

    fn send(&self, gl: &GL, index: &Index);
}
use super::super::math::Vector;

//Booleans
impl GLType for bool {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::BOOL };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform1i(Some(index), *self as i32);
    }
}

impl GLType for Vector<bool, 1> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::BOOL };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform1iv_with_i32_array(Some(index), self.clone().map(|x| x as i32).as_slice());
    }
}

impl GLType for Vector<bool, 2> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::BOOL_VEC2 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform2iv_with_i32_array(Some(index), self.clone().map(|x| x as i32).as_slice());
    }
}

impl GLType for Vector<bool, 3> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::BOOL_VEC3 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform3iv_with_i32_array(Some(index), self.clone().map(|x| x as i32).as_slice());
    }
}

impl GLType for Vector<bool, 4> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::BOOL_VEC4 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform4iv_with_i32_array(Some(index), self.clone().map(|x| x as i32).as_slice());
    }
}

//Singed integers
impl GLType for i32 {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::INT };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform1i(Some(index), *self as i32);
    }
}

impl GLType for Vector<i32, 1> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::INT };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform1iv_with_i32_array(Some(index), self.as_slice());
    }
}

impl GLType for Vector<i32, 2> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::INT_VEC2 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform2iv_with_i32_array(Some(index), self.as_slice());
    }
}

impl GLType for Vector<i32, 3> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::INT_VEC3 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform3iv_with_i32_array(Some(index), self.as_slice());
    }
}

impl GLType for Vector<i32, 4> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::INT_VEC4 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform4iv_with_i32_array(Some(index), self.as_slice());
    }
}

//Unsigned integers
impl GLType for u32 {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::UNSIGNED_INT };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform1ui(Some(index), *self);
    }
}

impl GLType for Vector<u32, 1> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::UNSIGNED_INT };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform1uiv_with_u32_array(Some(index), self.as_slice());
    }
}

impl GLType for Vector<u32, 2> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::UNSIGNED_INT_VEC2 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform2uiv_with_u32_array(Some(index), self.as_slice());
    }
}

impl GLType for Vector<u32, 3> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::UNSIGNED_INT_VEC3 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform3uiv_with_u32_array(Some(index), self.as_slice());
    }
}

impl GLType for Vector<u32, 4> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::UNSIGNED_INT_VEC4 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform4uiv_with_u32_array(Some(index), self.as_slice());
    }
}

//Floating points
impl GLType for f32 {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform1f(Some(index), *self);
    }
}

impl GLType for Vector<f32, 1> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform1fv_with_f32_array(Some(index), self.as_slice());
    }
}

impl GLType for Vector<f32, 2> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_VEC2 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform2fv_with_f32_array(Some(index), self.as_slice());
    }
}

impl GLType for Vector<f32, 3> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_VEC3 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform3fv_with_f32_array(Some(index), self.as_slice());
    }
}

impl GLType for Vector<f32, 4> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_VEC4 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform4fv_with_f32_array(Some(index), self.as_slice());
    }
}

use super::super::math::Matrix;
impl GLType for Matrix<f32, 2, 2> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_MAT2 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform_matrix2fv_with_f32_array(Some(index), true, self.as_slice());
    }
}

impl GLType for Matrix<f32, 2, 3> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_MAT2X3 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform_matrix2x3fv_with_f32_array(Some(index), true, self.as_slice());
    }
}

impl GLType for Matrix<f32, 2, 4> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_MAT2X4 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform_matrix2x4fv_with_f32_array(Some(index), true, self.as_slice());
    }
}

impl GLType for Matrix<f32, 3, 2> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_MAT3X2 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform_matrix3x2fv_with_f32_array(Some(index), true, self.as_slice());
    }
}

impl GLType for Matrix<f32, 3, 3> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_MAT3 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform_matrix3fv_with_f32_array(Some(index), true, self.as_slice());
    }
}

impl GLType for Matrix<f32, 3, 4> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_MAT3X4 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform_matrix3x4fv_with_f32_array(Some(index), true, self.as_slice());
    }
}

impl GLType for Matrix<f32, 4, 2> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_MAT4X2 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform_matrix4x2fv_with_f32_array(Some(index), true, self.as_slice());
    }
}

impl GLType for Matrix<f32, 4, 3> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_MAT4X3 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform_matrix4x3fv_with_f32_array(Some(index), true, self.as_slice());
    }
}

impl GLType for Matrix<f32, 4, 4> {
    const SIZE: i32 = 1;
    const TYPE: u32 = { GL::FLOAT_MAT4 };
    fn send(&self, gl: &GL, index: &Index) {
        gl.uniform_matrix4fv_with_f32_array(Some(index), true, self.as_slice());
    }
}
