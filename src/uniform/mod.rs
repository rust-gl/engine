use super::GL;
use std::rc::Rc;

pub mod base;
pub use base::*;

pub mod gltype;
pub use gltype::*;
use web_sys::{WebGlActiveInfo, WebGlProgram, WebGlUniformLocation};

type Index = WebGlUniformLocation;

#[derive(Debug, PartialEq, Eq)]
pub enum Member {
    Member(Option<Rc<Member>>, String),
    Index(Rc<Member>, usize),
}

use core::fmt;
impl fmt::Display for Member {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            Member::Member(prev, name) => {
                if let Some(x) = prev {
                    let _ = write! {fmt, "{}.", x}?;
                }
                write! {fmt, "{}", name}
            }
            Member::Index(prev, index) => write! {fmt, "{}[{}]", prev, index},
        }
    }
}

use std::collections::HashMap;

pub trait ToUniform {
    type Output;
    type Error;
    fn to_uniform(
        self,
        gl: Rc<GL>,
        program: &WebGlProgram,
        member: Option<Rc<Member>>,
        map: &mut HashMap<String, WebGlActiveInfo>,
    ) -> Result<Self::Output, Self::Error>;
}

pub trait Uniform<T> {
    fn index(&self) -> &Index;
    fn swap(&mut self, value: T) -> T;
}

impl ToUniform for () {
    type Output = ();
    type Error = !;
    fn to_uniform(
        self,
        _: Rc<GL>,
        _: &WebGlProgram,
        _: Option<Rc<Member>>,
        _: &mut HashMap<String, WebGlActiveInfo>,
    ) -> Result<Self::Output, Self::Error> {
        Ok(())
    }
}
