use super::*;
use derive_more::*;
use std::rc::Rc;

#[derive(Debug, Deref)]
pub struct Base<T> {
    index: Index,
    #[allow(unused)]
    name: String,
    gl: Rc<GL>,
    #[deref]
    value: T,
}

#[derive(Debug, PartialEq, Eq, Display)]
pub enum Error {
    GLError(super::super::context::Error),
    Type(gltype::Error),
    #[display(fmt = "Uniform '{}' does not exists", _0)]
    Uniform(String),
    #[display(fmt = "Uniform '{}' is not active", _0)]
    Active(String),
}

impl<T> Uniform<T> for Base<T>
where
    T: GLType,
{
    fn index(&self) -> &Index {
        &self.index
    }
    fn swap(&mut self, mut value: T) -> T {
        use core::mem::swap;
        value.send(&*self.gl, &self.index);
        swap(&mut self.value, &mut value);
        value
    }
}

impl<T> ToUniform for T
where
    T: GLType,
{
    type Output = Base<T>;
    type Error = Error;
    fn to_uniform(
        self,
        gl: Rc<GL>,
        program: &WebGlProgram,
        member: Option<Rc<Member>>,
        map: &mut HashMap<String, WebGlActiveInfo>,
    ) -> Result<Self::Output, Self::Error> {
        let name = member.map(|x| x.to_string()).unwrap_or_default();
        let index = match gl.get_uniform_location(program, &name) {
            Some(x) => Ok(x),
            _ => {
                use crate::context::GLError;
                gl.error().map_err(Error::GLError)?;
                Err(Error::Uniform(name.clone()))
            }
        }?;

        let info = map.remove(&name).ok_or(Error::Active(name.clone()))?;

        //Compare if the type is compatible with the attribute.
        T::compatible(&info).map_err(Error::Type)?;

        use core::mem::MaybeUninit;
        let mut base = Base::<T> {
            index,
            name,
            gl,
            value: unsafe { MaybeUninit::uninit().assume_init() },
        };

        base.swap(self);

        Ok(base)
    }
}
