use super::context::Context;
use super::program::Program;

///The current in-use program.
pub struct Using<'context, 'program, U, A> {
    pub context: &'context mut Context,
    pub program: &'program mut Program<U, A>,
}

use core::ops::*;

impl<'context, 'program, U, A> Deref for Using<'context, 'program, U, A> {
    type Target = Context;
    fn deref(&self) -> &Self::Target {
        &self.context
    }
}

impl Context {
    pub fn using<'context, 'program, U, A>(
        &'context mut self,
        program: &'program mut Program<U, A>,
    ) -> Using<'context, 'program, U, A> {
        self.use_program(Some(&program));
        Using {
            context: self,
            program,
        }
    }
}
