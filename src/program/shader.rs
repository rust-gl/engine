use derive_more::*;
use web_sys::WebGlShader;
///Simple webgl shader typed wrapper.
#[derive(Deref)]
pub struct Shader<const TYPE: u32>(WebGlShader);

pub trait ShaderCreator<const TYPE: u32> {
    type Error: std::error::Error;
    fn shader(&self, src: &str) -> Result<(Shader<TYPE>, String), Self::Error>;
}

#[derive(Debug, From, Display)]
pub enum Error {
    #[display(fmt = "Could not create shader")]
    ShaderCreation,
    #[display(fmt = "Could not retrive shader log")]
    ShaderLog,
    Compiler(String),
}

impl std::error::Error for Error {}

use web_sys::WebGl2RenderingContext;
impl<const TYPE: u32> ShaderCreator<TYPE> for WebGl2RenderingContext {
    type Error = Error;
    fn shader(&self, src: &str) -> Result<(Shader<TYPE>, String), Self::Error> {
        //Create the opengl shader.
        let shader = self.create_shader(TYPE).ok_or(Error::ShaderCreation)?;
        //Set the source of the shader.
        self.shader_source(&shader, src);
        //Compile the shader.
        self.compile_shader(&shader);

        //Get the shader compile log.
        let log = self.get_shader_info_log(&shader).ok_or(Error::ShaderLog)?;

        //Check for errors.
        let error = self
            .get_shader_parameter(&shader, WebGl2RenderingContext::COMPILE_STATUS)
            .is_truthy();

        //The log contains errors as well as warnings, so if there is any error or warning, return a compile error.
        if error {
            Ok((Shader(shader), log))
        } else {
            Err(Error::Compiler(log))
        }
    }
}

use super::Strict;
impl<const TYPE: u32> Strict for (Shader<TYPE>, String) {
    type Output = Shader<TYPE>;
    type Error = dyn std::error::Error;
    fn strict(self) -> Result<Self::Output, Box<Self::Error>> {
        let (shader, warnings) = self;
        if warnings.is_empty() {
            Ok(shader)
        } else {
            Err(warnings.into())
        }
    }
}
