use super::super::context::Context;
use super::Shader;
use web_sys::WebGl2RenderingContext;

pub type VertexShader = Shader<{ WebGl2RenderingContext::VERTEX_SHADER }>;
pub type FragmentShader = Shader<{ WebGl2RenderingContext::FRAGMENT_SHADER }>;

///Builds a webgl program given a context, vertex- and fragment-shader.
pub struct ProgramBuilder<'context, 'vertex, 'fragment, U = (), A = ()> {
    context: &'context Context,
    vertex_shader: &'vertex VertexShader,
    fragment_shader: &'fragment FragmentShader,
    uniforms: Option<U>,
    attributes: Option<A>,
}

impl<'context, 'vertex, 'fragment> Context {
    ///Constructs a program builder.
    pub fn program<U, A>(
        &'context self,
        vertex_shader: &'vertex VertexShader,
        fragment_shader: &'fragment FragmentShader,
    ) -> ProgramBuilder<'context, 'vertex, 'fragment, U, A> {
        ProgramBuilder {
            context: self,
            vertex_shader,
            fragment_shader,
            uniforms: None,
            attributes: None,
        }
    }
}

use derive_more::*;
#[derive(Debug, PartialEq, Eq, Display)]
pub enum Error<U, A> {
    Compile(String),
    #[display(fmt = "Could not get compile status")]
    CompileStatus,
    #[display(fmt = "Could not create program")]
    CreateProgram,
    #[display(fmt = "Attributes not set")]
    Attributes,
    ToAttribute(A),
    #[display(fmt = "Uniforms not set")]
    Uniforms,
    ToUniform(U),
    #[display(
        fmt = "Missing uniforms : {:?}, missing attributes : {:?}",
        uniforms,
        attributes
    )]
    Missing {
        uniforms: Vec<String>,
        attributes: Vec<String>,
    },
}

use std::error;
impl<U, A> error::Error for Error<U, A>
where
    U: error::Error,
    A: error::Error,
{
}

use super::super::attribute::ToAttribute;
use super::super::uniform::ToUniform;
use super::Program;

impl<'context, 'vertex, 'fragment, U, A> ProgramBuilder<'context, 'vertex, 'fragment, U, A> {
    pub fn attributes(mut self, attributes: A) -> Self {
        self.attributes = Some(attributes);
        self
    }
    pub fn uniforms(mut self, uniforms: U) -> Self {
        self.uniforms = Some(uniforms);
        self
    }

    ///Builds a webgl program from a program builder.
    pub fn build<'program, Uo, Ue, Ao, Ae>(self) -> Result<(Program<Uo, Ao>, String), Error<Ue, Ae>>
    where
        A: ToAttribute<Output = Ao, Error = Ae>,
        U: ToUniform<Output = Uo, Error = Ue>,
    {
        let ProgramBuilder {
            context,
            vertex_shader,
            fragment_shader,
            uniforms,
            attributes,
        } = self;

        let program = context.create_program().ok_or(Error::CreateProgram)?;

        context.attach_shader(&program, &*vertex_shader);
        context.attach_shader(&program, &*fragment_shader);
        context.link_program(&program);
        context.validate_program(&program);

        let warnings = context
            .get_program_info_log(&program)
            .ok_or(Error::CompileStatus)?;

        context.use_program(Some(&program));

        let num_uniforms = context
            .get_program_parameter(&program, WebGl2RenderingContext::ACTIVE_UNIFORMS)
            .as_f64()
            .unwrap() as u32;

        let mut map_uniforms = (0..num_uniforms)
            .flat_map(|index| {
                context
                    .get_active_uniform(&program, index)
                    .map(|x| (x.name().clone(), x))
            })
            .collect();

        let uniforms = uniforms
            .ok_or(Error::Uniforms)?
            .to_uniform(context.gl.clone(), &program, None, &mut map_uniforms)
            .map_err(Error::ToUniform)?;

        let num_attributes = context
            .get_program_parameter(&program, WebGl2RenderingContext::ACTIVE_ATTRIBUTES)
            .as_f64()
            .unwrap() as u32;

        let mut map_attributes = (0..num_attributes)
            .flat_map(|index| {
                context
                    .get_active_attrib(&program, index)
                    .map(|x| (x.name().clone(), x))
            })
            .collect();

        let attributes = attributes
            .ok_or(Error::Attributes)?
            .to_attribute(context.gl.clone(), &program, "", &mut map_attributes)
            .map_err(Error::ToAttribute)?;

        if !(map_uniforms.is_empty() && map_attributes.is_empty()) {
            Err(Error::Missing {
                uniforms: map_uniforms.into_keys().collect(),
                attributes: map_attributes.into_keys().collect(),
            })
        } else {
            let program = Program {
                program,
                gl: context.gl.clone(),
                uniforms,
                attributes,
            };

            Ok((program, warnings))
        }
    }
}

use super::Strict;
impl<Uo, Ao> Strict for (Program<Uo, Ao>, String) {
    type Output = Program<Uo, Ao>;
    type Error = dyn std::error::Error;
    fn strict(self) -> Result<Self::Output, Box<Self::Error>> {
        let (program, warnings) = self;
        if warnings.is_empty() {
            Ok(program)
        } else {
            Err(warnings.into())
        }
    }
}
