pub mod builder;
pub use builder::*;

pub mod shader;
pub use shader::*;

use super::GL;
use derive_more::*;
use std::rc::Rc;
use web_sys::WebGlProgram;

pub trait Strict {
    type Output;
    type Error: ?Sized + std::error::Error;
    fn strict(self) -> Result<Self::Output, Box<Self::Error>>;
}

#[derive(Deref, DerefMut)]
pub struct Program<U = (), A = ()> {
    #[deref]
    #[deref_mut]
    program: WebGlProgram,
    gl: Rc<GL>,
    pub uniforms: U,
    pub attributes: A,
}

impl<U, A> Drop for Program<U, A> {
    fn drop(&mut self) {
        self.gl.delete_program(Some(&self.program))
    }
}
